#![feature(asm)]
#![allow(dead_code)]
#![allow(non_snake_case)]

use std::ffi::CStr;
use winapi::{shared::{minwindef::HMODULE, ntdef::LPCSTR}, um::{libloaderapi::{GetModuleHandleA, LoadLibraryA}, winnt::{IMAGE_DIRECTORY_ENTRY_EXPORT, IMAGE_DOS_HEADER, IMAGE_DOS_SIGNATURE, IMAGE_EXPORT_DIRECTORY, IMAGE_NT_HEADERS, IMAGE_NT_SIGNATURE}}};

const MOD_ADLER32 : u32 = 65521;

#[allow(dead_code)]
pub const fn adler32(data : &[u8]) -> u32{
    /*
    Source:
    <https://en.wikipedia.org/wiki/Adler-32>
    */
    let mut result_a : u32 = 1; 
    let mut result_b : u32 = 0;
    
    let data_len : usize = data.len();
    let mut idx : usize = 0;

    loop {
        result_a = (result_a + (data[idx] as u32)) % MOD_ADLER32;
        result_b = (result_b + result_a) % MOD_ADLER32;
        idx += 1;
        if idx == data_len {
            break
        }
    }
    (result_b << 16) | result_a
}

pub struct PebLdr {
    pub module_base : usize,
    pub p_eat_ordtbl : *const u16,
    pub p_eat_strtbl : *const u32,
    pub p_eat_functbl : *const u32,
    pub num_exports : usize,
    pub has_init : bool
}

// todo:
// resolve GetModuleHandleA / LoadLibraryA during construction
// link normally for now
impl PebLdr {
    fn new(module_base : usize) -> PebLdr {
        let mut retVal = PebLdr {
            has_init: false,
            module_base: module_base,
            num_exports: 0,
            p_eat_functbl: 0 as *mut _,
            p_eat_ordtbl: 0 as *mut _,
            p_eat_strtbl: 0 as *mut _
        };
        retVal._eat_from_base();
        retVal
    }

    /// Initializes a PebLdr struct with the given base address of a loaded module.
    /// This is most useful when working with reflectively loaded dll's.
    /// Example:
    /// ```rust
    /// let h_module : usize = reflectively_load_library(library_data : &[u8]);
    /// let rdll_loader : pebldr_rs::PebLdr = match pebldr_rs::PebLdr::from_module_base(h_module){
    ///     Some(x) => x,
    ///     None => {
    ///         // there was an error properly parsing the DOS/NT headers 
    ///         // with a module at this given address
    ///         return
    ///     }
    /// };
    /// 
    /// const HASH_SOME_FUNC : u32 = pebldr_rs::adler32("SomeFunctionHereNoNullNeeded".as_bytes());
    /// 
    /// let virt_addr_some_func : usize = match rdll_loader.get_from_adler32(HASH_SOME_FUNC){
    ///     Some(x) => x as usize,
    ///     None => {
    ///         // the hash was not found when walking the module's export table
    ///         return
    ///     }
    /// };
    /// 
    pub fn from_module_base(_base : usize) -> Option<PebLdr>{
        let loader = PebLdr::new(_base as usize);
        
        if loader.has_init {
            Some(loader)
        } else {
            None
        }
    }

    /// Initializes a PebLdr struct with the given *NULL TERMINATED* string of a loaded or unloaded module.
    /// Example:
    /// ```rust
    /// let k32_loader : pebldr_rs::PebLdr = match pebldr_rs::PebLdr::from_module_name("Kernel32\0"){
    ///     Some(x) => x,
    ///     None => {
    ///         // there was an error properly parsing the DOS/NT headers 
    ///         // with a module at this given address
    ///         return
    ///     }
    /// };
    /// 
    /// const HASH_BEEP : u32 = pebldr_rs::adler32("Beep".as_bytes());
    /// 
    /// let virt_addr_some_func : usize = match k32_loader.get_from_adler32(HASH_BEEP){
    ///     Some(x) => x as usize,
    ///     None => {
    ///         // the hash was not found when walking the module's export table
    ///         return
    ///     }
    /// };
    /// 
    pub fn from_module_name(_module_name : &'static str) -> Option<PebLdr> {
        let mut h_module : HMODULE = unsafe{
            GetModuleHandleA(_module_name.as_ptr() as LPCSTR)
        };
        if (h_module as usize) == 0 {
            h_module = unsafe {
                LoadLibraryA(_module_name.as_ptr() as LPCSTR)
            };
            if (h_module as usize) == 0 {
                return None
            }
        }

        let loader = PebLdr::new(h_module as usize);
        
        if loader.has_init {
            Some(loader)
        } else {
            None
        }        
    }

    /// With the provided adler32 hash of a function name, this function attempts to walk
    /// the export address table of the module associated with the PebLdr struct
    /// and return the address of that function
    /// If the function does not exist, `None` is returned
    /// If the function is found, Some(*const u8) is returned
    pub fn get_from_adler32(&mut self, adler_hash : u32) -> Option<*const u8> {
        // get a slice from the raw pointers
        // I wonder if it'd be easier to just use std::ptr::add with std::mem::sizeof<T>()

        // potential future problem:
        // if num_exports * sizeof(T) for ordtbl and functbl this will fail / introduce instability
        // that may critically effect the process
        // see: https://doc.rust-lang.org/std/slice/fn.from_raw_parts.html#safety
        let slc_ord_tbl = unsafe { std::slice::from_raw_parts(self.p_eat_ordtbl, self.num_exports)};
        let slc_func_tbl = unsafe { std::slice::from_raw_parts(self.p_eat_functbl, self.num_exports)};

        // if we haven't init'd or any of the ptr tables are zero
        if !(self.has_init && self.p_eat_functbl as usize != 0 && self.p_eat_ordtbl as usize != 0 && self.p_eat_strtbl as usize != 0) {
            // we exit
            return None
        } 
        // otherwise, walk the exports
        else {
            let mut str_tbl_iter = self.p_eat_strtbl;

            for idx in 0..self.num_exports  {
                // get the dword pointed to by str_tbl_iter, aka the RVA
                let str_idx_rva = unsafe{ *str_tbl_iter };

                // add that to the module base to get the Virtual Address (va)
                let str_idx_va = str_idx_rva as usize + self.module_base;

                // get the CStr pointed to by str_idx_va
                let cstr_idx = unsafe {CStr::from_ptr(str_idx_va as *const i8)};
                
                // hash it
                let cstr_hash = adler32(cstr_idx.to_bytes());

                // if the hashes match, we found the function we want
                if cstr_hash == adler_hash {

                    // get the ordinal of the function
                    let fn_ord = slc_ord_tbl[idx];

                    // use that to index the function rva table
                    let fn_rva = slc_func_tbl[fn_ord as usize];

                    // add the rva to the module base to get the functions Virtual Address
                    let fn_va = fn_rva as usize + self.module_base;
                    return Some(fn_va as *const u8)
                }

                unsafe {
                    // if the hashes didn't match, increment the string table pointer
                    str_tbl_iter = str_tbl_iter.add(1);
                }
            }

            return None
        }
    }

    fn _eat_from_base(&mut self) -> bool {
        if self.module_base == 0 {
            return false
        }

        let lp_dos_hdr : *const IMAGE_DOS_HEADER = self.module_base as *const IMAGE_DOS_HEADER;
        let dos_hdr : IMAGE_DOS_HEADER;

        unsafe {
            dos_hdr = *lp_dos_hdr;
        }
        
        // sanity check e_magic
        if dos_hdr.e_magic != IMAGE_DOS_SIGNATURE {
            self.has_init = false;
            return false
        }

        let lp_nt_hdr : *const IMAGE_NT_HEADERS = (self.module_base + (dos_hdr.e_lfanew as usize)) as *const IMAGE_NT_HEADERS;
        let nt_hdr : IMAGE_NT_HEADERS;

        unsafe {
            nt_hdr = *lp_nt_hdr;
        }

        // sanity checking for PE 
        if nt_hdr.Signature != IMAGE_NT_SIGNATURE {
            self.has_init = false;
            return false
        }

        let lp_directory_export : *const IMAGE_EXPORT_DIRECTORY = (
            nt_hdr.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT as usize].VirtualAddress as usize + self.module_base
        ) as *const IMAGE_EXPORT_DIRECTORY;

        let directory_export : IMAGE_EXPORT_DIRECTORY;

        unsafe {
            directory_export = *lp_directory_export;
        }

        self.p_eat_strtbl = (self.module_base + directory_export.AddressOfNames as usize) as *const u32;
        self.p_eat_ordtbl = (self.module_base + directory_export.AddressOfNameOrdinals as usize) as *const u16;
        self.p_eat_functbl = (self.module_base + directory_export.AddressOfFunctions as usize) as *const u32;
        self.num_exports = directory_export.NumberOfFunctions as usize;

        self.has_init = true;
        true
    }

}

#[cfg(test)]
mod test {
    use crate::PebLdr;

    #[test]
    pub fn load_kernel32_beep(){
        const HASH : u32 = crate::adler32("Beep".as_bytes());
        let module_name = "Kernel32";

        let mut loader = match PebLdr::from_module_name(module_name){
            Some(x) => x,
            None => return
        };

        let _lp_beep = loader.get_from_adler32(HASH);
        println!("{:?}", _lp_beep);
    }

    #[test]
    pub fn load_kernel32_badfunc() {
        const HASH : u32 = crate::adler32("This function doesn't exist\0".as_bytes());
        let module_name = "Kernel32";

        let mut loader = match PebLdr::from_module_name(module_name){
            Some(x) => x,
            None => return
        };

        let _lp_beep = loader.get_from_adler32(HASH);
    }

    #[test]
    pub fn load_badmodule_badfunc() {
        const HASH : u32 = crate::adler32("This function doesn't exist\0".as_bytes());
        let module_name = "This Module doesn't exist\0";

        let mut loader = match PebLdr::from_module_name(module_name){
            Some(x) => x,
            None => return
        };

        let _lp_beep = loader.get_from_adler32(HASH);
    }

    #[test]
    pub fn test_1(){
        #[allow(non_upper_case_globals)]
        const cexpr_adler : u32 = crate::adler32("Nobody expects the spammish inquisition!\0".as_bytes());
        // println!(
        //     "EQ : {:?}", 
        //     cexpr_adler == adler::adler32("Nobody expects the spammish inquisition!\0".as_bytes())
        // )
    }

    #[test]
    pub fn test_2(){
        #[allow(unused_variables)]
        #[allow(non_upper_case_globals)]
        #[allow(dead_code)]
        const cexpr_adler : u32 = crate::adler32("This shouldn't appear in the binary!\0".as_bytes());
    }
}