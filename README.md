## PebLdr-rs

#### Abstract

PebLdr-rs is a transcoding of the [PebLdr](https://gist.github.com/alfarom256/4702614d8728e41851240e7320fa4d4a) project into Rust.

PebLdr-rs uses the 32 bit [Adler](https://en.wikipedia.org/wiki/Adler-32) function to generate both constexpr and runtime-evaluated hashes.


A PebLdr struct can be instantiated with the given *NULL TERMINATED* string of a loaded or unloaded module, or the base address of a module.

Example with a given module name string:

```rust
let k32_loader : pebldr_rs::PebLdr = match pebldr_rs::PebLdr::from_module_name("Kernel32\0"){
    Some(x) => x,
    None => {
        // there was an error properly parsing the DOS/NT headers 
        // with a module at this given address
        return
    }
};

const HASH_BEEP : u32 = pebldr_rs::adler32("Beep".as_bytes());

let virt_addr_some_func : usize = match k32_loader.get_from_adler32(HASH_BEEP){
    Some(x) => x as usize,
    None => {
        // the hash was not found when walking the module's export table
        return
    }
};
``` 

A PebLdr struct can _also_ be instantiated with the base address of a module, useful when dealing with reflectively loaded dlls or dlls manually mapped into a process.

Example:

```rust
let h_module : usize = reflectively_load_library(library_data : &[u8]);
let rdll_loader : pebldr_rs::PebLdr = match pebldr_rs::PebLdr::from_module_base(h_module){
    Some(x) => x,
    None => {
        // there was an error properly parsing the DOS/NT headers 
        // with a module at this given address
        return
    }
};

const HASH_SOME_FUNC : u32 = pebldr_rs::adler32("SomeFunctionHereNoNullNeeded".as_bytes());

let virt_addr_some_func : usize = match rdll_loader.get_from_adler32(HASH_SOME_FUNC){
    Some(x) => x as usize,
    None => {
        // the hash was not found when walking the module's export table
        return
    }
};
``` 